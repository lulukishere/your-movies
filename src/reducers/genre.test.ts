import reducer from './genre';
import * as actions from '../actions/genre';

describe('movie reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      error: null,
      loading: false,
      genres: [],
    });
  });

  it('should handle SEND_REQUEST', () => {
    expect(reducer(undefined, { type: actions.SEND_REQUEST }))
      .toEqual({
        error: null,
        loading: true,
        genres: [],
      });
  });

  it('should handle REQUEST_SUCCEEDED', () => {
    expect(reducer(
      undefined,
      {
        type: actions.REQUEST_SUCCEEDED,
        result: {
          error: null,
          loading: true,
          genres: [],
        },
      },
    ))
      .toEqual({
        error: null,
        loading: false,
        genres: [],
      });
  });

  it('should handle REQUEST_FAILED', () => {
    const error = new Error();

    expect(reducer(undefined, { type: actions.REQUEST_FAILED, error }))
      .toEqual({
        error,
        loading: false,
        genres: [],
      });
  });
});
