import {
  GenreProps,
  GenreAction,
  SEND_REQUEST,
  REQUEST_SUCCEEDED,
  REQUEST_FAILED,
} from '../actions/genre';

const initialValue: GenreProps = {
  error: null,
  loading: false,
  genres: [],
};

export default function reducer(
  state: GenreProps = initialValue,
  action: GenreAction,
) {
  switch (action.type) {
    case SEND_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };

    case REQUEST_SUCCEEDED:
      return {
        ...state,
        ...action.result,
        error: null,
        loading: false,
      };

    case REQUEST_FAILED:
      return {
        ...state,
        error: action.error,
        loading: false,
      };

    default:
      return state;
  }
}
