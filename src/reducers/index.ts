import { combineReducers, Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import movie from './movie';
import genre from './genre';
import favorite from './favorite';

const rootReducer = combineReducers({
  movie,
  genre,
  favorite,
});

export type RootState = ReturnType<typeof rootReducer>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export default rootReducer;
