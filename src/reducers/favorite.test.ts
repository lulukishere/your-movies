import reducer from './favorite';
import * as actions from '../actions/favorite';

describe('movie reducer', () => {
  it('should handle REQUEST_SUCCEEDED', () => {
    const itemMock = {
      id: 1,
      image: '',
      title: '',
      overview: '',
      releaseDate: '',
      voteAverage: 0
    };

    expect(reducer(undefined, { type: actions.TOGGLE_FAVORITE, id: 1, item: itemMock },
    ))
      .toEqual({
        error: null,
        data: { 1: { isFavorited: true, item: itemMock } },
      });
  });

  it('should handle CLEAR_FAVORITE', () => {
    expect(reducer(undefined, { type: actions.CLEAR_FAVORITE }))
      .toEqual({
        error: null,
        data: {},
      });
  });
});
