import {
  MovieProps,
  MovieAction,
  SEND_REQUEST,
  REQUEST_SUCCEEDED,
  REQUEST_FAILED,
  RESET,
} from '../actions/movie';

const initialValue: MovieProps = {
  error: null,
  loading: false,
  results: [],
  page: 0,
  totalPages: 0,
  totalResults: 0,
};

export default function reducer(
  state: MovieProps = initialValue,
  action: MovieAction,
) {
  switch (action.type) {
    case SEND_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };

    case REQUEST_SUCCEEDED:
      return {
        ...state,
        ...action.result,
        error: null,
        loading: false,
      };

    case REQUEST_FAILED:
      return {
        ...state,
        error: action.error,
        loading: false,
      };

    case RESET:
      return { ...initialValue };

    default:
      return state;
  }
}
