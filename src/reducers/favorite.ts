import {
  FavoriteProps,
  FavoriteAction,
  TOGGLE_FAVORITE,
  CLEAR_FAVORITE
} from '../actions/favorite';

const initialValue: FavoriteProps = {
  error: null,
  data: {},
};

export default function reducer(
  state: FavoriteProps = initialValue,
  action: FavoriteAction,
) {
  switch (action.type) {
    case TOGGLE_FAVORITE:
      return {
        ...state,
        data: {
          ...state.data,
          [action.id]: {
            isFavorited: !state.data[action.id]?.isFavorited,
            item: action.item,
          },
        },
      };

    case CLEAR_FAVORITE:
      return { ...initialValue };

    default:
      return state;
  }
}

export const getFavoriteCount = (state: object) => {
  return Object.values(state).reduce((acc, cur) => cur.isFavorited ? ++acc : acc, 0);
};
