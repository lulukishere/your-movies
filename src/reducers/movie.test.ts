import reducer from './movie';
import * as actions from '../actions/movie';

describe('movie reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      error: null,
      loading: false,
      results: [],
      page: 0,
      totalPages: 0,
      totalResults: 0,
    });
  });

  it('should handle SEND_REQUEST', () => {
    expect(reducer(undefined, { type: actions.SEND_REQUEST }))
      .toEqual({
        error: null,
        loading: true,
        results: [],
        page: 0,
        totalPages: 0,
        totalResults: 0,
      });
  });

  it('should handle REQUEST_SUCCEEDED', () => {
    expect(reducer(
      undefined,
      {
        type: actions.REQUEST_SUCCEEDED,
        result: {
          error: null,
          loading: true,
          results: [],
          page: 0,
          totalPages: 0,
          totalResults: 0,
        },
      },
    ))
      .toEqual({
        error: null,
        loading: false,
        results: [],
        page: 0,
        totalPages: 0,
        totalResults: 0,
      });
  });

  it('should handle REQUEST_FAILED', () => {
    const error = new Error();

    expect(reducer(undefined, { type: actions.REQUEST_FAILED, error }))
      .toEqual({
        error,
        loading: false,
        results: [],
        page: 0,
        totalPages: 0,
        totalResults: 0,
      });
  });

  it('should handle RESET', () => {
    expect(reducer(undefined, { type: actions.RESET }))
      .toEqual({
        error: null,
        loading: false,
        results: [],
        page: 0,
        totalPages: 0,
        totalResults: 0,
      });
  });
});
