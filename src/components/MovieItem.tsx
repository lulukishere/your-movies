import React from 'react';
import { Box, Image } from '@chakra-ui/react';
import { StarIcon, Icon } from '@chakra-ui/icons';
import { FaHeart } from 'react-icons/fa';
import dayjs from 'dayjs';

import './MovieItem.css';

interface MovieItemProps {
  id: number;
  imageUrl: string | null;
  title: string;
  releaseDate: string;
  voteAvarage: number;
  isFavorited?: boolean;
  onFavoriteClick?: (id: number) => void;
}

const MovieItem: React.FC<MovieItemProps> = (props: MovieItemProps) => {
  const imageUrl = !props.imageUrl
    ? 'https://via.placeholder.com/500x750?text=No Image'
    : `https://image.tmdb.org/t/p/w500${props.imageUrl}`;

  return (
    <Box maxW="sm" borderWidth="1px" borderRadius="lg" overflow="hidden" boxShadow="md">
      <Box className="image-wrapper">
        <Image src={imageUrl} alt={props.title} />
        <Box className="overlay" />
        <Box
          className={!props.isFavorited ? 'fav-button-anim' : ''}
          as="button"
          position="absolute"
          top="10px"
          right="15px"
          onClick={() => props.onFavoriteClick && props.onFavoriteClick(props.id)}
        >
          <Icon as={FaHeart} color={props.isFavorited ? 'red.500' : 'white'} boxSize={6} />
        </Box>
      </Box>
      <Box p="3">
        <Box d="flex" mt="2" alignItems="center">
          <StarIcon color="yellow.500" />
          <Box as="span" ml="2" fontWeight="bold" color="gray.400">
            {props.voteAvarage}
          </Box>
        </Box>
        <Box
          mt="1"
          fontWeight="semibold"
          as="h4"
          lineHeight="tight"
          isTruncated
        >
          {props.title}
        </Box>
        <Box
          color="gray.500"
          letterSpacing="wide"
          fontSize="xs"
        >
          {dayjs(props.releaseDate).format('MMM DD, YYYY')}
        </Box>
      </Box>
    </Box>
  );
};

export default MovieItem;
