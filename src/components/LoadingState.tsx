import React from 'react';
import { Box, Skeleton, SkeletonText } from '@chakra-ui/react';

const LoadingState = () => {
  return (
    <Box maxW="sm" borderWidth="1px" borderRadius="lg" overflow="hidden" boxShadow="md">
      <Skeleton height="200px" />
      <SkeletonText p="3" noOfLines={3} spacing="4" />
    </Box>
  );
};

export default LoadingState;