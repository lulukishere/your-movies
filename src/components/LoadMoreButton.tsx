import React from 'react';
import { Box, Spinner } from '@chakra-ui/react';

export interface LoadMoreButtonProps {
  title?: string;
  loading?: boolean;
  onClick?: () => void;
}

const LoadMoreButton: React.FC<LoadMoreButtonProps> = ({
  title = 'Load More',
  loading,
  onClick
}: LoadMoreButtonProps) => {
  return (
    <Box
      as="button"
      onClick={onClick}
      w="100%"
      mt={[2, 5, 7]}
      borderRadius="sm"
      bg="purple.700"
      color="white"
      px={4}
      fontWeight="bold"
      h={10}
    >
      {loading ? <Spinner size="md" /> : title}
    </Box>
  );
};

export default LoadMoreButton;
