import React, { useState } from 'react';
import { Flex, Heading, Box, FlexProps } from '@chakra-ui/react';
import { HamburgerIcon } from '@chakra-ui/icons';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../reducers';
import { getFavoriteCount } from '../reducers/favorite';
import { FavoriteProps } from '../actions/favorite';

import { ColorModeSwitcher } from '../ColorModeSwitcher';
import MenuItem from '../components/MenuItem';

interface HeaderProps {
  favorite: FavoriteProps;
}

const Header: React.FC<FlexProps & HeaderProps> = (props: FlexProps & HeaderProps) => {
  const [visible, setVisible] = useState<boolean>(false);

  const handleToggle = () => setVisible(!visible);

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1rem"
      bg="purple.900"
      color="white"
      {...props}
    >
      <Flex align="center" mr={5}>
        <Heading as="h1" size="lg" letterSpacing="-.1rem">
          Your Movies
        </Heading>
      </Flex>
      <Box display={{ base: 'block', md: 'none' }} onClick={handleToggle}>
        <HamburgerIcon />
      </Box>
      <Box
        display={{ sm: visible ? 'block' : 'none', md: 'flex' }}
        width={{ sm: 'full', md: 'auto' }}
        alignItems="center"
        flexGrow={1}
      >
        <Link to="/">
          <MenuItem>Home</MenuItem>
        </Link>
        <Link to="/favorites">
          <MenuItem>Favorites ({getFavoriteCount(props.favorite.data)})</MenuItem>
        </Link>
      </Box>
      <Box
        display={{ sm: visible ? 'block' : 'none', md: 'block' }}
        mt={{ base: 4, md: 0 }}
      >
        <ColorModeSwitcher />
      </Box>
    </Flex>
  );
};

const mapStateToProps = ({ favorite }: RootState) => ({ favorite });

export default connect(mapStateToProps)(Header);
