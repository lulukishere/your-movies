import {
  createStore,
  applyMiddleware,
  MiddlewareAPI,
  Dispatch,
  Action,
} from 'redux';
import thunkMiddleware from 'redux-thunk-recursion-detect';

import rootReducer from './reducers';
import { loadState, saveState } from './utils/localstorage';

const STORAGE_KEY = '_favorites';

const persistedState = { favorite: loadState(STORAGE_KEY) };

const myMiddleware = (store: MiddlewareAPI) => (next: Dispatch) => (action: Action) => {
  const result = next(action);

  if (result.type === 'favorite::TOGGLE_FAVORITE') {
    saveState(store.getState().favorite, STORAGE_KEY);
  }

  return result;
}
const store = createStore(
  rootReducer,
  persistedState,
  applyMiddleware(thunkMiddleware, myMiddleware),
);

export default store;
