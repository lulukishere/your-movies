import { MovieItemProps } from './movie';

export interface FavoriteProps {
  error: null;
  data: {
    [key: string]: {
      isFavorited: boolean;
      item: MovieItemProps;
    };
  };
}

export const TOGGLE_FAVORITE = 'favorite::TOGGLE_FAVORITE';
export type TOGGLE_FAVORITE_TYPE = typeof TOGGLE_FAVORITE;
export interface ToggleFavoriteProps {
  type: TOGGLE_FAVORITE_TYPE;
  id: number;
  item: MovieItemProps;
}

export function toggleFavorite(id: number, item: MovieItemProps): ToggleFavoriteProps {
  return { type: TOGGLE_FAVORITE, id, item };
}

export const CLEAR_FAVORITE = 'favorite::CLEAR_FAVORITE';
export type CLEAR_FAVORITE_TYPE = typeof CLEAR_FAVORITE;
export interface ClearFavoriteProps {
  type: CLEAR_FAVORITE_TYPE;
}

export function clearFavorite(): ClearFavoriteProps {
  return { type: CLEAR_FAVORITE };
}

export type FavoriteAction = | ToggleFavoriteProps | ClearFavoriteProps;
