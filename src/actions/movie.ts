import { ThunkDispatch as Dispatch } from 'redux-thunk';

import { AppThunk } from '../reducers';
import { client } from '../utils';

export interface MovieItemProps {
  id: number;
  image: string;
  title: string;
  overview: string;
  releaseDate: string;
  voteAverage: number;
}

export interface MovieProps {
  error: Error | null;
  loading: boolean;
  results: MovieItemProps[];
  page: number;
  totalPages: number;
  totalResults: number;
}

export const SEND_REQUEST = 'movie::SEND_REQUEST';
export type SEND_REQUEST_TYPE = typeof SEND_REQUEST;
export interface SendRequestProps {
  type: SEND_REQUEST_TYPE;
}

export function sendRequest(): SendRequestProps {
  return { type: SEND_REQUEST };
}

export const REQUEST_SUCCEEDED = 'movie::REQUEST_SUCCEEDED';
export type REQUEST_SUCCEEDED_TYPE = typeof REQUEST_SUCCEEDED;
export interface RequestSucceededProps {
  type: REQUEST_SUCCEEDED_TYPE;
  result: MovieProps;
}

export function requestSucceeded(result: any): RequestSucceededProps {
  return { type: REQUEST_SUCCEEDED, result };
}

export const REQUEST_FAILED = 'movie::REQUEST_FAILED';
export type REQUEST_FAILED_TYPE = typeof REQUEST_FAILED;
export interface RequestFailedProps {
  type: REQUEST_FAILED_TYPE;
  error: Error;
}

export function requestFailed(error: Error): RequestFailedProps {
  return { type: REQUEST_FAILED, error };
}

export const RESET = 'movie::RESET';
export type RESET_TYPE = typeof RESET;
export interface ResetProps {
  type: RESET_TYPE;
}

export function reset(): ResetProps {
  return { type: RESET };
}

export type MovieAction =
| SendRequestProps
| RequestSucceededProps
| RequestFailedProps
| ResetProps;

export interface MovieRequestParams {
  sortBy?: 'popularity.desc' | 'release_date.desc';
  withGenres?: string;
}

export function loadMovie(params?: MovieRequestParams): AppThunk {
  return async (dispatch: Dispatch<MovieAction, {}, any>, getState) => {
    const { movie } = getState();

    dispatch(sendRequest());

    try {
      const res = await client.get('/discover/movie', {
        params: {
          language: 'en-US',
          sort_by: params?.sortBy || 'popularity.desc',
          with_genres: params?.withGenres || undefined,
          include_adult: false,
          include_video: false,
          page: movie.page || 1,
        }
      });

      const modifiedResults = [];

      for (let i = 0, len = res.data.results.length; i < len; i++) {
        const item = res.data.results[i];
        modifiedResults.push({
          image: item.poster_path,
          releaseDate: item.release_date,
          voteAverage: item.vote_average,
          ...item,
        });
      }

      dispatch(requestSucceeded({
        ...modifiedResults,
        results: movie.page === 1
          ? modifiedResults : [...movie.results, ...modifiedResults],
        page: res.data.page + 1,
        totalPages: res.data.total_pages,
        totalResults: res.data.total_results,
      }));
    } catch (err) {
      dispatch(requestFailed(err));
    }
  };
}
