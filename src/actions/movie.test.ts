import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk-recursion-detect';
import fetchMock from 'fetch-mock';

import * as actions from './movie';

describe('actions', () => {
  it('should create an action to initialize a request', () => {
    const expectedAction = {
      type: actions.SEND_REQUEST,
    };

    expect(actions.sendRequest()).toEqual(expectedAction);
  });

  it('should create an action to add list of movie', () => {
    const result: [] = [];
    const expectedAction = {
      type: actions.REQUEST_SUCCEEDED,
      result,
    };

    expect(actions.requestSucceeded(result)).toEqual(expectedAction);
  });

  it('should create an action throw an error occurred', () => {
    const error = new Error();
    const expectedAction = {
      type: actions.REQUEST_FAILED,
      error,
    };

    expect(actions.requestFailed(error)).toEqual(expectedAction);
  });

  it('should create an action to reset movie list', () => {
    const expectedAction = {
      type: actions.RESET,
    };

    expect(actions.reset()).toEqual(expectedAction);
  });
});

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('creates REQUEST_SUCCEEDED when fetching movies has been done', () => {
    fetchMock.getOnce('/movies', {
      headers: { 'content-type': 'application/json' },
      body: { page: 1, results: [] },
    });

    const initialState = {
      movie: { results: [], page: 0 },
    };
    const store = mockStore(initialState);

    const expectedActions = [
      { type: actions.SEND_REQUEST },
      {
        type: actions.REQUEST_SUCCEEDED,
        result: { page: 1, results: [] },
      },
    ];

    store.dispatch(actions.loadMovie())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });
  });
});