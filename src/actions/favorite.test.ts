import * as actions from './favorite';

describe('actions', () => {
  it('should create a favorite', () => {
    const itemMock = {
      id: 1,
      image: '',
      title: '',
      overview: '',
      releaseDate: '',
      voteAverage: 0
    };

    const expectedAction = {
      type: actions.TOGGLE_FAVORITE,
      id: 1,
      item: itemMock,
    };

    expect(actions.toggleFavorite(1, itemMock)).toEqual(expectedAction);
  });

  it('should clear favorited movies', () => {
    const expectedAction = {
      type: actions.CLEAR_FAVORITE,
    };

    expect(actions.clearFavorite()).toEqual(expectedAction);
  });
});
