import { ThunkDispatch as Dispatch } from 'redux-thunk';

import { AppThunk } from '../reducers';
import { client } from '../utils';

export interface GenreItemProps {
  id: number;
  name: string;
}

export interface GenreProps {
  error: Error | null;
  loading: boolean;
  genres: GenreItemProps[];
}

export const SEND_REQUEST = 'genre::SEND_REQUEST';
export type SEND_REQUEST_TYPE = typeof SEND_REQUEST;
export interface SendRequestProps {
  type: SEND_REQUEST_TYPE;
}

export function sendRequest(): SendRequestProps {
  return { type: SEND_REQUEST };
}

export const REQUEST_SUCCEEDED = 'genre::REQUEST_SUCCEEDED';
export type REQUEST_SUCCEEDED_TYPE = typeof REQUEST_SUCCEEDED;
export interface RequestSucceededProps {
  type: REQUEST_SUCCEEDED_TYPE;
  result: GenreProps;
}

export function requestSucceeded(result: any): RequestSucceededProps {
  return { type: REQUEST_SUCCEEDED, result };
}

export const REQUEST_FAILED = 'genre::REQUEST_FAILED';
export type REQUEST_FAILED_TYPE = typeof REQUEST_FAILED;
export interface RequestFailedProps {
  type: REQUEST_FAILED_TYPE;
  error: Error;
}

export function requestFailed(error: Error): RequestFailedProps {
  return { type: REQUEST_FAILED, error };
}

export type GenreAction =
| SendRequestProps
| RequestSucceededProps
| RequestFailedProps;

export function loadGenre(): AppThunk {
  return async (dispatch: Dispatch<GenreAction, {}, any>) => {
    dispatch(sendRequest());

    try {
      const res = await client.get('/genre/movie/list', {
        params: { language: 'en-US' }
      });

      dispatch(requestSucceeded({ genres: res.data.genres }));
    } catch (err) {
      dispatch(requestFailed(err));
    }
  };
}
