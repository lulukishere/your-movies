import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk-recursion-detect';
import fetchMock from 'fetch-mock';

import * as actions from './genre';

describe('actions', () => {
  it('should create an action to initialize a request', () => {
    const expectedAction = {
      type: actions.SEND_REQUEST,
    };

    expect(actions.sendRequest()).toEqual(expectedAction);
  });

  it('should create an action to add list of genre', () => {
    const result: [] = [];
    const expectedAction = {
      type: actions.REQUEST_SUCCEEDED,
      result,
    };

    expect(actions.requestSucceeded(result)).toEqual(expectedAction);
  });

  it('should create an action throw an error occurred', () => {
    const error = new Error();
    const expectedAction = {
      type: actions.REQUEST_FAILED,
      error,
    };

    expect(actions.requestFailed(error)).toEqual(expectedAction);
  });
});

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('creates REQUEST_SUCCEEDED when fetching genres has been done', () => {
    fetchMock.getOnce('/movies', {
      headers: { 'content-type': 'application/json' },
      body: { genres: [] },
    });

    const initialState = {};
    const store = mockStore(initialState);

    const expectedActions = [
      { type: actions.SEND_REQUEST },
      {
        type: actions.REQUEST_SUCCEEDED,
        result: { genres: [] },
      },
    ];

    store.dispatch(actions.loadGenre())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });
  });
});