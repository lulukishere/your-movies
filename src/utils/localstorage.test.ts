import { loadState, saveState } from './localstorage';

describe('localstorage', () => {
  it('should return undefined', () => {
    expect(loadState('key')).toBeUndefined();
  });
});

describe('localstorage', () => {
  it('should no return', () => {
    expect(saveState({}, 'key')).toBeUndefined();
  });
});

describe('localstorage', () => {
  it('should return an object', () => {
    expect(loadState('key')).toEqual({});
  });
});
