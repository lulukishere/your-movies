import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
  params: { api_key: '150fb00f27a441a64e02ca05c4e9ae12' },
});

export default instance;
