export const loadState = (key: string) => {
  try {
    const serialized = localStorage.getItem(key);

    if (serialized === null) {
      return undefined;
    }

    return JSON.parse(serialized);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (item: any, key: string) => {
  try {
    const serialized = JSON.stringify(item);

    localStorage.setItem(key, serialized);

  } catch (err) {
    // 
  }
};
