import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../reducers';
import { toggleFavorite } from '../../actions/favorite';

import FavoriteList from './FavoriteList';

const mapStateTopProps = ({ favorite }: RootState) => ({
  favorite,
});
const mapDispatchToProps = {
  toggleFavorite,
};

export default withRouter(
  connect(mapStateTopProps, mapDispatchToProps)(FavoriteList)
);
