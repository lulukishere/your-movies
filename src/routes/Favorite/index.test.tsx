import React from 'react';

import { render, screen } from '../../utils/test';
import * as favoriteActions from '../../actions/favorite';

import Favorite from './';

import store from '../../store';

describe('connected redux component', () => {
  it('renders the connected component with initialState', () => {
    render(<Favorite />, {
      initialState: {
        favorite: { error: null, data: {} },
        toggleFavorite: favoriteActions.toggleFavorite,
      },
      store
    });

    expect(screen.getByText(/There is no favorites yet/i)).toBeInTheDocument();
  });
});
