import React, { useCallback } from 'react';
import {
  Box,
  Text,
  SimpleGrid,
  Alert,
  AlertIcon,
} from '@chakra-ui/react';

import Header from '../../components/Header';
import MovieItem from '../../components/MovieItem';

import { MovieItemProps } from '../../actions/movie';
import { FavoriteProps } from '../../actions/favorite';

interface FavoriteListProps {
  favorite: FavoriteProps;
  toggleFavorite: (id: number, item: MovieItemProps) => void;
}

const FavoriteList: React.FC<FavoriteListProps> = ({
  favorite,
  toggleFavorite,
}: FavoriteListProps) => {
  const handleFavoriteClick = useCallback((id: number, item: MovieItemProps) => {
    toggleFavorite(id, item);
  }, [toggleFavorite])

  const favorites = Object.values(favorite.data).filter((o) => o.isFavorited);

  return (
    <>
      <Header />
      <Box ml={[2, 5, 10]} mt={[2, 3, 5]} mr={[2, 5, 10]} mb={[2, 3, 5]}>
        <Text fontSize="4xl">Favorites</Text>
      </Box>
      {!favorites.length && (
          <Box ml={[2, 5, 10]} mt={[2, 3, 5]} mr={[2, 5, 10]}>
            <Alert status="info">
              <AlertIcon />
              There is no favorites yet
            </Alert>
          </Box>
        )}
      <SimpleGrid
        columns={{ sm: 1, md: 2, lg: 5 }}
        spacingX="40px"
        spacingY="35px"
        ml={[2, 5, 10]}
        mr={[2, 5, 10]}
        mb={[2, 5, 10]}
      >
        {favorites.map((favItem) => (
        <MovieItem
          key={favItem.item.id}
          id={favItem.item.id}
          imageUrl={favItem.item.image}
          title={favItem.item.title}
          releaseDate={favItem.item.releaseDate}
          voteAvarage={favItem.item.voteAverage}
          isFavorited={favItem.isFavorited}
          onFavoriteClick={(id) => handleFavoriteClick(id, favItem.item)}
        />))}
      </SimpleGrid>
    </>
  );
};

export default FavoriteList;