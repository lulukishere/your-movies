import React from 'react';
import { Container, Heading, Text } from '@chakra-ui/react';

import Header from '../components/Header';

const NotFound: React.FC<{}> = () => {
  return (
    <>
      <Header />
      <Container centerContent>
        <Heading size="4xl">404</Heading>
        <Text>Page Not Found</Text>
      </Container>
    </>
  );
};

export default NotFound;
