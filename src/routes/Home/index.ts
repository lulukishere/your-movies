import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../reducers';
import { loadMovie, reset } from '../../actions/movie';
import { loadGenre } from '../../actions/genre';
import { toggleFavorite } from '../../actions/favorite';

import MovieList from './MovieList';

const mapStateTopProps = ({ movie, genre, favorite }: RootState) => ({
  movie,
  genre,
  favorite,
});
const mapDispatchToProps = {
  loadMovie,
  resetMovie: reset,
  loadGenre,
  toggleFavorite,
};

export default withRouter(
  connect(mapStateTopProps, mapDispatchToProps)(MovieList)
);
