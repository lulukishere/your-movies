import React from 'react';
import { Provider } from 'react-redux';
import TestRenderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk-recursion-detect';

import * as genreActions from '../../actions/genre';
import * as movieActions from '../../actions/movie';
import * as favoriteActions from '../../actions/favorite';

import Home from './';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('connected redux component', () => {
  let store: any, component: any;

  beforeEach(() => {
    store = mockStore({
      movie: {
        error: null,
        loading: false,
        results: [],
        page: 0,
      },
      loadMovie: movieActions.loadMovie,
      resetMovie: movieActions.reset,
      genre: {
        error: [],
        loading: false,
        genres: [],
      },
      loadGenre: genreActions.loadGenre,
      favorite: { error: null, data: {} },
      toggleFavorite: favoriteActions.toggleFavorite,
    });

    component = TestRenderer.create(
      <Provider store={store}>
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      </Provider>
    );
  });

  it('should render with given state from redux store', () => {
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should dispatch an action on select change', () => {
    TestRenderer.act(() => {
      component.root.findByType('select').props.onChange({ target: { value: 1 } });
    });

    expect(jest.fn(store.dispatch)).toHaveBeenCalledTimes(1);
    expect(jest.fn(store.dispatch)).toHaveBeenCalledWith(genreActions.loadGenre());
  });
});
