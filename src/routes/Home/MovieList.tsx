import React, { useState, useEffect, useCallback } from 'react';
import {
  Flex,
  Box,
  Text,
  Select,
  SimpleGrid,
} from '@chakra-ui/react';

import Header from '../../components/Header';
import MovieItem from '../../components/MovieItem';
import LoadMoreButton from '../../components/LoadMoreButton';
import LoadingState from '../../components/LoadingState';

import { MovieProps, MovieItemProps, MovieRequestParams } from '../../actions/movie';
import { GenreProps, GenreItemProps } from '../../actions/genre';
import { FavoriteProps } from '../../actions/favorite';

interface MovieListProps {
  movie: MovieProps;
  loadMovie: (params?: MovieRequestParams) => void;
  resetMovie: () => void;
  genre: GenreProps,
  loadGenre: () => void;
  favorite: FavoriteProps;
  toggleFavorite: (id: number, item: MovieItemProps) => void;
}

const MovieList: React.FC<MovieListProps> = ({
  movie,
  loadMovie,
  resetMovie,
  genre,
  loadGenre,
  favorite,
  toggleFavorite,
}: MovieListProps) => {
  const [selectedGenre, setSelectedGenre] = useState<string | undefined>();

  useEffect(() => {
    resetMovie();
    loadMovie();
  }, [loadMovie, resetMovie]);

  useEffect(() => {
    loadGenre();
  }, [loadGenre]);

  const handleGenreChange = useCallback((e: React.ChangeEvent<HTMLSelectElement>) => {
    resetMovie();

    if (e.target.value === 'release_date.desc') {
      loadMovie({ sortBy: 'release_date.desc' });
    } else {
      loadMovie({ withGenres: e.target.value });
      setSelectedGenre(e.target.value);
    }
  }, [loadMovie, resetMovie]);

  const handleLoadMore = useCallback(() => {
    loadMovie({ withGenres: selectedGenre });
  }, [loadMovie, selectedGenre]);

  const handleFavoriteClick = (id: number, item: MovieItemProps) => {
    toggleFavorite(id, item);
  }

  return (
    <>
      <Header />
      <Box ml={[2, 5, 10]} mt={[2, 3, 5]} mr={[2, 5, 10]} mb={[2, 3, 5]}>
        <Text fontSize="4xl">Discover</Text>
      </Box>
      <Flex>
        <Box w="200px" ml={10}>
          <Select isFullWidth onChange={handleGenreChange}>
            <option value="release_date.desc">Latest</option>
            {genre.genres.map((genreItem: GenreItemProps) => (
              <option key={genreItem.id} value={genreItem.id}>{genreItem.name}</option>
            ))}
          </Select>
        </Box>
        <Box flex={1} ml={[2, 5, 10]} mr={[2, 5, 10]} mb={[2, 5, 10]}>
          <SimpleGrid columns={{ sm: 1, md: 2, lg: 4 }} spacingX="40px" spacingY="35px">
            {movie.loading && !movie.results.length && (new Array(8)).fill(8).map((item, index) => (
              <LoadingState key={index} />
            ))}
            {movie.results.map((movieItem: MovieItemProps) => (
            <MovieItem
              key={movieItem.id}
              id={movieItem.id}
              imageUrl={movieItem.image}
              title={movieItem.title}
              releaseDate={movieItem.releaseDate}
              voteAvarage={movieItem.voteAverage}
              isFavorited={favorite.data[movieItem.id]?.isFavorited}
              onFavoriteClick={(id) => handleFavoriteClick(id, movieItem)}
            />))}
          </SimpleGrid>
          {movie.results.length > 0 && (
            <LoadMoreButton
              loading={movie.results.length > 0 && movie.loading}
              onClick={handleLoadMore}
            />
          )}
        </Box>
      </Flex>
    </>
  );
};

export default MovieList;
