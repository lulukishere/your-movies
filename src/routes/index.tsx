import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './Home';
import Favorite from './Favorite';
import NotFound from './404';

const Routes: React.FC<{}> = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/favorites" exact component={Favorite} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default Routes;
