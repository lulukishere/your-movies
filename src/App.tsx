import * as React from 'react';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';
import Routes from './routes';
import history from './utils/history';

const theme = extendTheme({
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false,
  },
});

export const App = () => (
  <Provider store={store}>
    <ChakraProvider theme={theme}>
      <Router history={history}>
        <Route component={Routes} />
      </Router>
    </ChakraProvider>
  </Provider>
);
